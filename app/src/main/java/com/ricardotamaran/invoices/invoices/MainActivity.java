package com.ricardotamaran.invoices.invoices;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonContinue = findViewById(R.id.buttonContinue);
        final EditText editTextUser = findViewById(R.id.editTextUser);
        final EditText editTextPassword = findViewById(R.id.editTextPassword);
        final TextView textViewMessage = findViewById(R.id.textViewMessage);

        buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Limpiamos el textView de mensajes
                textViewMessage.setText("");

                if( editTextUser.getText().toString().trim().length() == 0 || editTextPassword.getText().toString().trim().length() == 0 ){
                    textViewMessage.setText("No deje campos vacíos");
                }else if( !editTextUser.getText().toString().trim().equals("demo") ){
                    textViewMessage.setText("Usuario incorrecto");
                }else if( !editTextPassword.getText().toString().trim().equals("1234") ){
                    textViewMessage.setText("Contraseña incorrecta");
                }else{
                    Intent intent = new Intent(getApplicationContext(), GeneralActivity.class);
                    startActivity(intent);
                }

            }
        });
    }
}
